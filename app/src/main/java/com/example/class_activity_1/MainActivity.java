package com.example.class_activity_1;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toast.makeText(getApplicationContext(),"Hello World",Toast.LENGTH_SHORT).show();
//Customized
        Toast smd =  new Toast(getApplicationContext());
        String msg="Hello SMD";
        smd.setGravity(Gravity.CENTER ,0,0);
        smd.setDuration(Toast.LENGTH_SHORT);
   TextView textView=new TextView(MainActivity.this);
   textView.setTextColor(Color.WHITE);

   textView.setBackgroundColor(Color.parseColor("#1F394E"));
    textView.setText(msg);
        textView.setTextSize(25);
        textView.setPadding(15,15,15,15);
    smd.setView(textView);
        smd.show();




    }
}